import requests
from bs4 import BeautifulSoup
import csv

url = 'https://github.com/trending'
response = requests.get(url)

# Menampung hasil parse html ke variable soup
soup = BeautifulSoup(response.text, "html.parser")
# print(soup.prettify())

# Menampung hasil seleksi ke variable box_row
box_row = soup.findAll('article', class_='Box-row')
# print(box_row)

# Mempersiapkan variable untuk menampung array
github_author = []
github_link = []
github_programminglang = []
github_project = []

# Testing
# for a_link in box_row:
#     row_heading = a_link.find('h1')
#     row_a = row_heading.find('a')
#     print(row_a.prettify())

# Save Github Author
for a_link in box_row:
    row_heading = a_link.find('h1')
    row_a = row_heading.find('a')
    
    # Save Github Author to github_author var
    github_author.append(row_a.find('span').text.strip().replace(" /", ""))
    # print("Author :",row_a.find('span').text.strip().replace(" /", ""))

# Save Github Link
for a_link in box_row:
    row_heading = a_link.find('h1')
    row_a = row_heading.find('a')
    
    # Cek jika link tersedia
    if(row_a.get('href') != None):
        githublink = "https://github.com"+row_a.get('href') 
    else:
        githublink = "No Link Found"

    # Save Github Link to github_link var
    github_link.append(githublink)
    # print("Link :",githublink)

# Save Programming Language
for a_link in box_row:
    row_heading = a_link.find('h1')
    row_a = row_heading.find('a')
    
    row_programlang = a_link.find('span', attrs={'itemprop': 'programmingLanguage'})
    # Cek jika bahasa pemrograman tersedia
    if(row_programlang != None):
        program_lang = row_programlang.text
    else:
        program_lang = "-"
        
    # Save Github Programming Lang to github_programminglang var
    github_programminglang.append(program_lang)    
    # print("Programming Lang :",program_lang)

# Save Project Name
for a_link in box_row:
    row_heading = a_link.find('h1')
    row_a = row_heading.find('a')
    
    # Remove Author from row_a HTML
    row_a.find('span').decompose()

    # Save Github Project name to github_project var
    github_project.append(row_a.text.strip())
    # print("Project :",row_a.text.strip())

# Testing
# index = 0
# for author in github_author:
#     print("Author :", author)
#     print("Project Name :", github_project[index])
#     print("Programming Lang :", github_programminglang[index])
#     print("Description :", github_description[index])
#     print("Link :", github_link[index])
#     print("=================================================\n")
#     index += 1

# # for link in github_link:
# #     print(link)
# # for programlang in github_programminglang:
# #     print(programlang)
# # for project in github_project:
# #     print(project)

# Export to CSV
# Newline = '' used to remove extra blank row
f = csv.writer(open('github.csv', 'w', newline=''))
f.writerow(['Author', 'Project Name', 'Programming Language', 'Link'])

index = 0
for author in github_author:
    f.writerow([author, github_project[index], github_programminglang[index], github_link[index]])
    index += 1
    
print("Done")